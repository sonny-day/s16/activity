console.log("Hello World");


let num = Number(prompt("Please enter a number"));

for(let x = num; x >= 0; x-- ){

	if(x % 10 === 0){
		console.log("This number is divisible by 10 so we're skipping the number.")
		continue;
	}

	if(x % 5 === 0){
		console.log("This number is divisible by 5 so we're skipping the number.")
		continue;
	}

	if(x <= 50){
		console.log("STOP RIGHT NOW. THANK YOU VERY MUCH!")
		break;
	}

	console.log(x);
}

console.log('%cThe results below are from the stretch goals activity:%c supercalifragilisticexpialidocious','color: #ff33dd; font-size: 14px','font-weight: bold; font-size: 16px; color: red')


let word = "supercalifragilisticexpialidocious";

for(let y = 0; y < word.length; y++){
	if(

		word[y].toLowerCase() =="a" ||
		word[y].toLowerCase() =="e" ||
		word[y].toLowerCase() =="i" ||
		word[y].toLowerCase() =="o" ||
		word[y].toLowerCase() =="u" 

		){

		let vowels = word[y];
		console.log("Continue to the next iteration");
		continue;
	}

	if(

		word[y].toLowerCase() !="a" &&
		word[y].toLowerCase() !="e" &&
		word[y].toLowerCase() !="i" &&
		word[y].toLowerCase() !="o" &&
		word[y].toLowerCase() !="u" 

	) {

		let consonants = word[y];
		console.log(consonants);
		continue;
	}

	else {
		break;
	}
}



